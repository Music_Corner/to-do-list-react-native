import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function DeleteButton(props) {
    const {id} = props;
    const {index} = props;
    const {DeleteBlock} = props;
    return(
        <TouchableHighlight
        style={styles.DeleteButton}
        onPress={function(){DeleteBlock(index, id)}}
        id={id}
        name="Delete_Button">
            <Text style={styles.textButton}>
                Delete
            </Text>
        </TouchableHighlight>
    )
}

export default DeleteButton

const styles = StyleSheet.create({
    DeleteButton: {
        backgroundColor: '#ff7f50',
        width: 80,
        height: 40,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        justifyContent: "center",
        alignItems: 'center',
    },

    textButton: {
        fontSize: 18,
        fontStyle: 'italic',
        color: '#3a180e'
    }
})