import React from 'react';
import {StyleSheet, ScrollView, ActivityIndicator, View, Alert} from 'react-native';
import TitleBlockWithButtons from '../TitleBlockWithButtons/TitleBlockWithButtons';
import InputWithButton from '../InputWithButton/InputWithButton';
import ContentBlock from '../ContentBlock/ContentBlock';

class Container extends React.Component {
    constructor(props) {
        super(props)
       
        this.state = {
            myValue: '',
            newValue: '',
            items: [],
            RequestUrl: 'http://192.168.0.104:80/todoAPI',
            token: this.props.Token,
            preloaderActive: false
        }

        this.AddBlock = this.AddBlock.bind(this);
        this.ChangeHandler = this.ChangeHandler.bind(this);
        this.EditChangeHandler = this.EditChangeHandler.bind(this);
        this.DeleteBlock = this.DeleteBlock.bind(this);
        this.MarkBlock = this.MarkBlock.bind(this);
        this.LogoutClick = this.LogoutClick.bind(this);
        this.EditClick = this.EditClick.bind(this);
        this.RefreshClick = this.RefreshClick.bind(this);
        this.EditSaveClick = this.EditSaveClick.bind(this);
        this.EditCancelClick = this.EditCancelClick.bind(this);
    }

    // updateData() {
    //     this.setState({
    //         items: [...this.state.items, ...data[0].text],
    //         marked: [...this.state.marked, ...data[0].marked]
    //     });
    //     console.log(this.state)
    // }

    componentWillMount() {
        const url = this.state.RequestUrl + '/application/php/ajax/getData.php'
        let formData = new FormData;
        formData.append('token', this.props.Token);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = (data) => {
            if (data.data != null) {
                this.setState({
                    items: data.data
                })
            } else {
                this.setState({
                    items: [...this.state.items]
                })
            }
        }

        this.request(url, requestProps, success)

    }

    request(url, requestProps, success) {
        console.log(url)
        this.setState({preloaderActive: true})
        setTimeout(() => {
            fetch(url, requestProps).then(results => { return results.json() }).then(data => {
                this.setState({preloaderActive: false})
                console.log('response: ' + data.authStatus);
                if (data.authStatus == true) {
                    success(data)
                } else {
                    this.props.Auth(false)
                }
            }).catch((error) => {
                this.setState({preloaderActive: false})
                console.log('Error: ' + error + ' Message: ' + error.message)
                Alert.alert(
                    'Error',
                    'Network error',
                    [
                      {text: 'OK', onPress: () => console.log('clicked!')},
                    ],
                    { cancelable: false }
                )
            });
        }, 500)
    }

    AddBlock() {
        let id = 0;
        if (this.state.items.length > 0) {
            id = +this.state.items[this.state.items.length - 1].id + 1
        } else {
            id = +this.state.items.length
        }
        const url = this.state.RequestUrl + '/application/php/ajax/ajax.php'
        let formData = new FormData;
        formData.append('text', this.state.myValue);
        formData.append('dataindex', id);
        formData.append('token', this.props.Token);

        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            this.setState({
                items: [...this.state.items, {
                    text: this.state.myValue,
                    id: id,
                    marked: false,
                    inEdit: false
                }],
                myValue: '',
            })
        }

        this.request(url, requestProps, success)
    }

    DeleteBlock(index, idFromBlock) {
        console.log(index)
        const url = this.state.RequestUrl + '/application/php/ajax/ajaxDelete.php';
        let formData = new FormData;
        formData.append('dataindex', idFromBlock);
        formData.append('token', this.props.Token);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            if (index == 0) {
                this.state.items.shift();
            } else if (index > 0) {
                this.state.items.splice(index, 1)
            }

            this.setState({
                items: [...this.state.items],
            })
        }

        this.request(url, requestProps, success)
    }

    MarkBlock(index, idFromBlock) {
        console.log('id: ' + index + ' idFromBlock: ' + idFromBlock)
        let formData = new FormData;
        formData.append('dataindex', idFromBlock);
        formData.append('token', this.props.Token);
        let url = '';
            if (this.state.items[index].marked == true) {
                url = this.state.RequestUrl + '/application/php/ajax/ajaxMark.php';
            } else {
                url = this.state.RequestUrl + '/application/php/ajax/ajaxUnmark.php';
            }
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            this.state.items[index].marked = !this.state.items[index].marked;
            this.setState({
                items: [...this.state.items],
            })
        }
        this.request(url, requestProps, success)
    }

    LogoutClick() {
        const url = this.state.RequestUrl + '/application/php/auth/authReset.php';
        let formData = new FormData;
        formData.append('token', this.props.Token);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            //Useless)))
        }

        this.request(url, requestProps, success)
    }

    RefreshClick() {
        const url = this.state.RequestUrl + '/application/php/ajax/getData.php'
        let formData = new FormData;
        formData.append('token', this.props.Token);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = (data) => {
            if (data.data != null) {
                this.setState({
                    items: data.data
                })
            } else {
                this.setState({
                    items: [...this.state.items]
                })
            }
        }

        this.request(url, requestProps, success)
    }

    EditClick(index) {
        this.state.items[index].inEdit = true
        this.setState({
            newValue: this.state.items[index].text
        })
    }

    EditChangeHandler(index, value) {
        this.setState({newValue: value});
    }

    EditSaveClick(index, idFromBlock) {
        const url = this.state.RequestUrl + '/application/php/ajax/ajaxUpdateText.php';
        let formData = new FormData;
        formData.append('token', this.props.Token);
        formData.append('dataindex', idFromBlock);
        formData.append('updatedText', this.state.newValue);

        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = () => {
            this.state.items[index].inEdit = false;
            this.state.items[index].text = this.state.newValue;
            this.setState({
                items: [...this.state.items],
                newValue: ''
            })
        }
        this.request(url, requestProps, success);
    }

    EditCancelClick(index, idFromBlock) {
        this.state.items[index].inEdit = false;
        this.setState({
            items: [...this.state.items],
            newValue: ''
        })
    }

    ChangeHandler(value) {
        this.setState({myValue: value});
    }

    render() {
        const contentBlock = this.state.items.map((item, index) => {
            if (this.state.items[index] != null) {
                return (
                    <ContentBlock
                    key={index}
                    marked={this.state.items[index].marked}
                    DeleteBlock={this.DeleteBlock}
                    MarkBlock={this.MarkBlock}
                    index={index}
                    id={this.state.items[index].id}
                    text={this.state.items[index].text}
                    newValue={this.state.newValue}
                    EditChangeHandler={this.EditChangeHandler}
                    EditClick={this.EditClick}
                    inEdit={this.state.items[index].inEdit}
                    EditSaveClick={this.EditSaveClick}
                    EditCancelClick={this.EditCancelClick} />
                );
            }
        });

        if(this.state.preloaderActive == true) {
            return(
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        } else {

            return(
                <ScrollView 
                scrollEnabled={true}
                contentContainerStyle={styles.container}>
                <TitleBlockWithButtons RefreshClick={this.RefreshClick} LogoutClick={this.LogoutClick}/>
                <InputWithButton
                value={this.state.myValue}
                AddBlock = {this.AddBlock}
                ChangeHandler = {this.ChangeHandler}
                />
                {contentBlock}
                </ScrollView>
            )
        }
    }
}

export default Container

const styles = StyleSheet.create({
    container: {
        minHeight: '100%',
        paddingVertical: 5,
        paddingBottom: 30,
        paddingTop: 30,
        backgroundColor: '#ffa07a',
        alignItems: 'center',
        borderWidth: 5,
        borderColor: '#FF6347',
      },

      preloader: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#ffa07a',
      }
})