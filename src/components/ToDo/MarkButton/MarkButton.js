import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function MarkButton(props) {
    const {id} = props;
    const {index} = props;
    const {MarkBlock} = props;
    const {marked} = props;
    let title = 'Mark';
    if (marked === true) {
        title = 'Unmark'
    }
    return(
        <TouchableHighlight
        style={styles.MarkButton}
        onPress={function(){MarkBlock(index, id)}}
        id={id}
        name="Mark_Button">
            <Text style={styles.textButton}>
                {title}
            </Text>
        </TouchableHighlight>
    )
}


export default MarkButton

const styles = StyleSheet.create({
    MarkButton: {
        backgroundColor: '#ff7f50',
        width: 80,
        height: 40,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        justifyContent: "center",
        alignItems: 'center',
    },

    textButton: {
        fontSize: 18,
        fontStyle: 'italic',
        color: '#3a180e'
    }
})