import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function EditCancelButton(props) {
    const {EditCancelClick} = props,
    {id} = props,
    {index} = props

    return(
        <TouchableHighlight
        onPress={() => {EditCancelClick(index, id)}}
        style={styles.EditCancelButton}>
            <Text style={styles.text}>✕</Text>
        </TouchableHighlight>
    )
}

export default EditCancelButton

const styles = StyleSheet.create({
    EditCancelButton: {
        position: 'absolute',
        bottom: -2,
        right: -2,
        backgroundColor: '#ff7f50',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        alignItems:'center',
        justifyContent:'center',
        width: 37,
        height: 37,
    },

    text: {
        fontSize: 30,
        // fontStyle: 'italic',
        fontWeight: '800',
        lineHeight: 35,
        color: '#3a180e',
    }
})