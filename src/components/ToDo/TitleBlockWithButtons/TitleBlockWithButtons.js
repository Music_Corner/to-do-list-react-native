import { View, StyleSheet } from 'react-native';
import React, {Component} from 'react';
import TitleBlock from '../TitleBlock/TitleBlock';
import LogoutButton from '../LogoutButton/LogoutButton';
import RefreshButton from '../RefreshButton/RefreshButton';

function TitleBlockWithButton(props) {
    const {LogoutClick} = props,
    {RefreshClick} = props
    return(
        <View style={styles.block}>
            <LogoutButton LogoutClick={LogoutClick}/>
            <TitleBlock title="To Do"/>
            <RefreshButton RefreshClick={RefreshClick}/>
        </View>
    )
}

export default TitleBlockWithButton

const styles = StyleSheet.create({
    block: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 335
    }
})