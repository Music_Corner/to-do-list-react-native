import React from 'react';
import { AsyncStorage } from "react-native";
import ToDo from '../ToDo/ToDo';
import Login from '../Login/Login';

class Router extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loggedIn: false,
            token: ''
        }

        this.Auth = this.Auth.bind(this)
    }

    async componentWillMount() {
        try {
            const localStorageToken = await AsyncStorage.getItem('token')
            console.log(localStorageToken)
            if(localStorageToken != null) {
                this.setState({
                    loggedIn: true,
                    token: localStorageToken
                });
            } else {
                this.setState({
                    loggedIn: false,
                    token: ''
                });
            }
        } catch(error) {
            console.log(error);
        }
    }

    async Auth(result, token) {
        try {
            if (result == false) {
                this.setState({
                    loggedIn: result,
                    token: ''
                })
            } else {
                await AsyncStorage.setItem('token', token);
                const localStorageToken = await AsyncStorage.getItem('token')

                console.log('Token from auth func: ' + token)
                this.setState({
                    loggedIn: result,
                    token: token
                })
            }
        
        } catch(error) {
            console.log(error);
        }
}

    render () {
        if (this.state.loggedIn) {
            return(
                <ToDo Auth={this.Auth} Token={this.state.token} />
            )
        } else {
            return (
                <Login Auth={this.Auth} />
            )
        }
    }
}

export default Router
