import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function LoginButton(props) {
    const {LoginClick} = props;
    return(
        <TouchableHighlight onPress={LoginClick} style={styles.LoginButton}>
            <Text style={styles.textButton}>Login</Text>
        </TouchableHighlight>
    )
}

export default LoginButton

const styles = StyleSheet.create({
    LoginButton: {
        backgroundColor: '#ff7f50',
        width: 150,
        height: 50,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-start',
    },

    textButton: {
        fontSize: 20,
        fontStyle: 'italic',
    }
})