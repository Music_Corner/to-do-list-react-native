import {Text, ScrollView, View, StyleSheet } from 'react-native';
import React, {Component} from 'react';
import EditButton from '../EditButton/EditButton';

function TextBlock(props) {
    const {text} = props,
    {marked} = props,
    {EditClick} = props,
    {index} = props
    // {id} = props,

    if (marked === false) {
        return(
            <View style={styles.textBlock}>
                <ScrollView
                contentContainerStyle={styles.scrollView}
                showsVerticalScrollIndicator={false}>
                    <Text style={styles.defaultText}>
                        {text}
                    </Text>
                </ScrollView>
                <EditButton
                EditClick={EditClick}
                index={index}/>
            </View>
        )
    } else if (marked === true) {
        return(
            <View style={styles.textBlock}>
                <ScrollView
                contentContainerStyle={styles.scrollView}
                showsVerticalScrollIndicator={false}>
                    <Text style={styles.marked}>
                        {text}
                    </Text>
                </ScrollView>
                <EditButton
                EditClick={EditClick}
                index={index}/>
            </View>
        )
    }
}

export default TextBlock

const styles = StyleSheet.create({
    textBlock: {
        backgroundColor: '#ff7f50',
        height: 100,
        width: 210,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        top: 0,
    },

    defaultText: {
        fontSize: 20,
        fontStyle: 'italic',
        color: '#3a180e'
    },

    marked: {
        textDecorationLine: 'line-through',
        fontSize: 20,
        fontStyle: 'italic',
        color: '#3a180e' 
    },

    scrollView: {
        width: 210,
        minHeight: 1,
        justifyContent: "center",
        alignItems: 'center',
        padding: 10
    }
})