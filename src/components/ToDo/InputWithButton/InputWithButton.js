import {View, StyleSheet } from 'react-native';
import React, {Component} from 'react';
import AddButton from '../AddButton/AddButton';
import Input from '../Input/Input';

function InputWithButton(props) {
    const {AddBlock} = props;
    const {ChangeHandler} = props;

    return(
            <View style={styles.inputWithButton}>
                <Input ChangeHandler={ChangeHandler} value={props.value}/>
                <AddButton AddBlock={AddBlock} />
            </View>
    )
}

export default InputWithButton

const styles = StyleSheet.create({
    inputWithButton: {
        marginTop: 20,
        backgroundColor: '#ff7f50',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        padding: 10
    }
})