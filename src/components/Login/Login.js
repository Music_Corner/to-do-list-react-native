import React from 'react';
import Container from './Container/Container';

function Login(props) {
    const {Auth} = props
    return(
        <Container Auth={Auth}/>
    )
}

export default Login