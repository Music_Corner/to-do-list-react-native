import {TextInput, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function Input(props) {
    const {ChangeHandler} = props;
    const {value} = props;
    return(
        <TextInput style={styles.textInput}
        editable={true}
        name="input-name"
        onChangeText={(text) => {ChangeHandler(text)}}
        value={value}
        placeholder="Enter Something"
        placeholderTextColor= '#ffbaa5'
        />
    )
}

export default Input

const styles = StyleSheet.create({
    textInput: {
        width: 230,
        height: 50,
        backgroundColor: '#ff7f50',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        paddingLeft: 5,
        fontSize: 18,
        fontStyle: 'italic',
        color: '#3a180e'
        
    }
})