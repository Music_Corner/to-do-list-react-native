import React from 'react';
import {Text} from 'react-native';
import { Font } from 'expo';

export default class FontsComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fontLoaded: 0,
        };
    }

    async componentDidMount() {
        console.log('component did mount')
        try {
            this.setState({
                fontLoaded: 'lol'
            })
            await Font.loadAsync({
                'segoepr': require('../../../assets/fonts/segoepr.ttf'),
            });
            this.setState({
                fontLoaded: 'lol'
            })
        } catch(error) {
            console.log('error: ' + error)
        }
    }

    render() {
      console.log(this.state)
        if (this.state.fontLoaded == 1) {
            return(
                <Text style={{ fontFamily: 'segoepr' }}>Lololo</Text>
            )
        }
    }
}