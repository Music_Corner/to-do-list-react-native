import {Text, View, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function TitleBlock(props) {
    const {title} = props
        return(
            <View style={styles.titleBlock}>
                <Text style={styles.textBlock}>{title.toUpperCase()}</Text>
            </View>
        )
}

export default TitleBlock

const styles = StyleSheet.create({
    titleBlock: {
        borderColor: 'black',
        borderWidth: 2,
        backgroundColor: '#ff7f50',
        borderRadius: 10,
        borderColor: '#99492c',
        width: 180,
        height: 50,
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center'
    },

    textBlock: {
        // fontFamily: "segoepr",
        fontSize: 30,
        fontStyle: 'italic',
        fontWeight: '500',
        color: '#512112',
    }
})