import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function LogoutButton(props) {
    const {LogoutClick} = props;
    return(
        <TouchableHighlight onPress={LogoutClick} style={styles.logoutButton}>
            <Text style={styles.textButton}>⟵</Text>
        </TouchableHighlight>
    )
}

export default LogoutButton

const styles = StyleSheet.create({
    logoutButton: {
        backgroundColor: '#ff7f50',
        width: 70,
        height: 50,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: '#99492c'
    },

    textButton: {
        fontSize: 40,
        // fontStyle: 'italic',
        fontWeight: '800',
        lineHeight: 36,
        color: '#3a180e'
    }
})