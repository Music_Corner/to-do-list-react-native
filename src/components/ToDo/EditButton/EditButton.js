import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function EditButton(props) {
    const {id} = props,
    {index} = props,
    {EditClick} = props

    return (
        <TouchableHighlight style={styles.EditButton} onPress={() => {{EditClick(index)}}} >
            <Text style={styles.text}>✎</Text>
        </TouchableHighlight>
    )
}

export default EditButton;

const styles = StyleSheet.create({
    EditButton: {
        position: 'absolute',
        top: -2,
        right: -2,
        backgroundColor: '#ff7f50',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        transform:[{ rotateY: '180deg' }],
    },

    text: {
        fontSize: 35,
        // fontStyle: 'italic',
        fontWeight: '800',
        lineHeight: 35,
        color: '#3a180e',
    }
})