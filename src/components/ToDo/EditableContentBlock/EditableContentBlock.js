import {View, TextInput, StyleSheet } from 'react-native';
import React, {Component} from 'react';
import EditSaveButton from '../EditSaveButton/EditSaveButton';
import EditCancelButton from '../EditCancelButton/EditCancelButton';

function EditableContentBlock(props) {
    const {EditChangeHandler} = props,
    {EditSaveClick} = props,
    {EditCancelClick} = props,
    {newValue} = props,
    {index} = props,
    {id} = props

    return(
        <View style={styles.EditableContentBlock}>
            <TextInput
            style={styles.Input}
            onChangeText={(text) => {EditChangeHandler(index, text)}}
            value={newValue}/>
            
            <EditSaveButton
            EditSaveClick={EditSaveClick}
            index={index}
            id={id} />

            <EditCancelButton
            EditCancelClick={EditCancelClick}
            index={index}
            id={id} />
        </View>
    )
}

export default EditableContentBlock;

const styles = StyleSheet.create({
    EditableContentBlock: {
        backgroundColor: '#ff7f50',
        height: 100,
        width: 310,
        marginTop: 40,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        alignItems:'center',
        justifyContent:'center',
    },

    Input: {
        fontSize: 20,
        fontStyle: 'italic',
        color: '#3a180e'
    }
})