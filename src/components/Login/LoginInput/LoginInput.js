import {TextInput, StyleSheet, View } from 'react-native';
import React, {Component} from 'react';

function LoginInput(props) {
    const {LoginInputHandler} = props;
    const {value} = props;
    return(
        <View style={styles.inputBlock}>
            <TextInput style={styles.textInput}
            editable={true}
            name="input-name"
            onChangeText={(text) => {LoginInputHandler(text)}}
            value={value}
            placeholder="Enter your login"
            />
        </View>
    )
}

export default LoginInput

const styles = StyleSheet.create({
    textInput: {
        width: 300,
        height: 50,
        backgroundColor: '#ff7f50',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        paddingLeft: 5,
        fontSize: 18,
        fontStyle: 'italic',
        
    },

    inputBlock: {
        marginTop: 30,
        backgroundColor: '#ff7f50',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        padding: 10
    }
})