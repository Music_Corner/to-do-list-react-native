import {Button, View, StyleSheet } from 'react-native';
import React, {Component} from 'react'
import DeleteButton from '../DeleteButton/DeleteButton';
import MarkButton from '../MarkButton/MarkButton';

function Buttons(props) {
    const {id} = props;
    const {index} = props;
    const {DeleteBlock} = props;
    const {MarkBlock} = props;
    const {marked} = props;

    return(
        <View style={styles.buttonsBlock}>
            <MarkButton marked={marked} MarkBlock={MarkBlock} id={id} index={index}/>
            <DeleteButton DeleteBlock={DeleteBlock} id={id} index={index}/>
        </View>
    )
}

export default Buttons

const styles = StyleSheet.create({
    buttonsBlock: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginLeft: 20,
    }
})