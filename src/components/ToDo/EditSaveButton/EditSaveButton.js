import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function EditSaveButton(props) {
    const {EditSaveClick} = props,
    {index} = props,
    {id} = props
    return(
        <TouchableHighlight
        onPress={() => {EditSaveClick(index, id)}}
        style={styles.EditSaveButton}>
            <Text style={styles.text}>✓</Text>
        </TouchableHighlight>
    )
}

export default EditSaveButton

const styles = StyleSheet.create({
    EditSaveButton: {
        position: 'absolute',
        top: -2,
        right: -2,
        backgroundColor: '#ff7f50',
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        alignItems:'center',
        justifyContent:'center',
        width: 37,
        height: 37
    },

    text: {
        fontSize: 30,
        // fontStyle: 'italic',
        fontWeight: '800',
        lineHeight: 35,
        color: '#3a180e',
    }
})