import {Button, View, StyleSheet } from 'react-native';
import React, {Component} from 'react'
import TextBlock from '../TextBlock/TextBlock';
import Buttons from '../Buttons/Buttons';
import EditableContentBlock from '../EditableContentBlock/EditableContentBlock';

function ContentBlock(props) {
    const {text} = props,
    {id} = props,
    {index} = props,
    {DeleteBlock} = props,
    {MarkBlock} = props,
    {marked} = props,
    {EditClick} = props,
    {inEdit} = props,
    {newValue} = props,
    {EditChangeHandler} = props,
    {EditSaveClick} = props,
    {EditCancelClick} = props

    if(inEdit == true) {
        return(
            <EditableContentBlock
            id={id}
            index={index}
            EditChangeHandler={EditChangeHandler}
            EditSaveClick={EditSaveClick}
            EditCancelClick={EditCancelClick}
            newValue={newValue}/>
        )
    } else {
        return(
            <View style={styles.contentBlock} data-index={id} index={index}>
                <TextBlock
                marked={marked}
                text={text}
                id={id}
                index={index}
                EditClick={EditClick}/>
                
                <Buttons
                DeleteBlock={DeleteBlock}
                id={id}
                index={index}
                marked={marked}
                MarkBlock={MarkBlock} />
            </View>
        )
    } 
}

export default ContentBlock

const styles = StyleSheet.create({
    contentBlock: {
        marginTop: 40,
        height: 100,
        flexDirection: 'row',
    }
})