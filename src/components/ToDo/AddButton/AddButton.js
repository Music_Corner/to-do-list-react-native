import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function AddButton(props) {
    const {AddBlock} = props;
    return(
        <TouchableHighlight onPress={AddBlock} style={styles.addButton}>
            <Text style={styles.textButton}>Post</Text>
        </TouchableHighlight>
    )
}

export default AddButton

const styles = StyleSheet.create({
    addButton: {
        backgroundColor: '#ff7f50',
        width: 70,
        height: 50,
        marginLeft: 10,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        justifyContent: 'center',
        alignItems: 'center'
    },

    textButton: {
        fontSize: 20,
        fontStyle: 'italic',
        color: '#3a180e'
    }
})