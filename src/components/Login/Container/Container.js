import React from 'react';
import {Text, View, StyleSheet, ActivityIndicator, Alert} from 'react-native';
import TitleBlock from '../../ToDo/TitleBlock/TitleBlock';
import LoginInput from '../LoginInput/LoginInput';
import PasswordInput from '../PasswordInput/PasswordInput';
import Buttons from '../Buttons/Buttons';


class Container extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: false,
            requestUrl: 'http://192.168.0.104:80/todoAPI',
            form: {
                login: '',
                password: ''
            },
            preloaderActive: false
        }

        this.LoginClick = this.LoginClick.bind(this);
        this.RegisterClick = this.RegisterClick.bind(this);
        this.LoginInputHandler = this.LoginInputHandler.bind(this);
        this.PasswordInputHandler = this.PasswordInputHandler.bind(this);

    }

    request(url, requestProps, success) {
        this.setState({preloaderActive: true})
        setTimeout(() => {
            fetch(url, requestProps).then(results => { return results.json() }).then(data => {
                console.log('token from response login: ' + data.token)
                this.setState({preloaderActive: false})
                if (data.authStatus == true) {
                    success(data)
                } else {
                    Alert.alert(
                        'Incorrect form',
                        'Login or password is incorrect or already exists. ' +
                        'Try again please',
                        [
                          {text: 'OK', onPress: () => console.log('clicked!')},
                        ],
                        { cancelable: false }
                    )
                    this.props.Auth(false)
                }
            }).catch((error) => {
                this.setState({preloaderActive: false})
                console.log('Error: ' + error)
                Alert.alert(
                    'Error',
                    'Network problem',
                    [
                      {text: 'OK', onPress: () => console.log('clicked!')},
                    ],
                    { cancelable: false }
                )
                this.props.Auth(false)
            });
        }, 500)
    }

    LoginClick() {
        const url = this.state.requestUrl + '/application/php/ajax/ajaxLogin.php';
        let formData = new FormData;
        formData.append('login', this.state.form.login);
        formData.append('password', this.state.form.password);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = (data) => {
            this.props.Auth(data.authStatus, data.token)
        }
        
        this.request(url, requestProps, success)
    }

    RegisterClick() {
        const url = this.state.requestUrl + '/application/php/ajax/ajaxRegister.php';
        let formData = new FormData;
        formData.append('login', this.state.form.login);
        formData.append('password', this.state.form.password);
        const requestProps = {
            method: 'POST',
            body: formData
        }

        const success = (data) => {
            this.props.Auth(data.authStatus, data.token)
        }
        
        this.request(url, requestProps, success)
    }

    LoginInputHandler(value) {
        this.setState({
            form: {
                login: value,
                password: this.state.form.password
            }    
        });
    }

    PasswordInputHandler(value) {
        this.setState({
            form: {
                login: this.state.form.login,
                password: value
            }    
        });
    }

    render() {
        if(this.state.preloaderActive == true) {
            return(
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        } else {
            return (
                <View style={styles.container}>
                    <TitleBlock title="Login"/>
                    <LoginInput
                    value={this.state.form.login}
                    LoginInputHandler={this.LoginInputHandler} />

                    <PasswordInput
                    value={this.state.form.password}
                    PasswordInputHandler={this.PasswordInputHandler} />

                    <Buttons
                    LoginClick={this.LoginClick}
                    RegisterClick={this.RegisterClick} />
                </View>
            )
        }
    }
}

export default Container

const styles = StyleSheet.create({
    container: {
        minHeight: '100%',
        paddingVertical: 5,
        paddingBottom: 30,
        paddingTop: 30,
        backgroundColor: '#ffa07a',
        alignItems: 'center',
        borderWidth: 5,
        borderColor: '#FF6347',
        flexDirection: 'column',
      },

      preloader: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#ffa07a',
      }

})