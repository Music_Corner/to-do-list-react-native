import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function RegisterButton(props) {
    const {RegisterClick} = props;
    return(
        <TouchableHighlight onPress={RegisterClick} style={styles.LoginButton}>
            <Text style={styles.textButton}>Register</Text>
        </TouchableHighlight>
    )
}

export default RegisterButton

const styles = StyleSheet.create({
    LoginButton: {
        backgroundColor: '#ff7f50',
        width: 150,
        height: 50,
        borderWidth: 2,
        borderRadius: 10,
        borderColor: '#99492c',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end'
    },

    textButton: {
        fontSize: 20,
        fontStyle: 'italic',
    }
})