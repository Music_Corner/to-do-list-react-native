import React from 'react';
import Router from './src/components/Router/Router';
import { Font } from 'expo';

export default class App extends React.Component {
  render() {
    return (
      <Router />
    );
  }

}