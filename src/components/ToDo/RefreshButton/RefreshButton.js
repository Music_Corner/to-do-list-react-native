import {Text, TouchableHighlight, StyleSheet } from 'react-native';
import React, {Component} from 'react';

function RefreshButton(props) {
    const {RefreshClick} = props;
    return(
        <TouchableHighlight onPress={RefreshClick} style={styles.refreshButton}>
            <Text style={styles.textButton}>⟳</Text>
        </TouchableHighlight>
    )
}

export default RefreshButton

const styles = StyleSheet.create({
    refreshButton: {
        backgroundColor: '#ff7f50',
        width: 70,
        height: 50,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: '#99492c'
    },

    textButton: {
        fontSize: 40,
        // fontStyle: 'italic',
        fontWeight: '800',
        lineHeight: 40,
        color: '#3a180e'
    }
})