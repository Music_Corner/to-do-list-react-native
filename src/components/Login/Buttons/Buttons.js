import {View, StyleSheet } from 'react-native';
import React, {Component} from 'react';
import LoginButton from './LoginButton/LoginButton'
import RegisterButton from './RegisterButton/RegisterButton';

function Buttons(props) {
    const {LoginClick} = props;
    const {RegisterClick} = props;
    return (
        <View style={styles.buttons}>
            <LoginButton LoginClick={LoginClick} />
            <RegisterButton RegisterClick={RegisterClick} />
        </View>
    )
}

export default Buttons

const styles = StyleSheet.create({
    buttons: {
        flexDirection: 'row',
        marginTop: 50,
        justifyContent: 'space-between',
        width: 320,
        position: 'absolute',
        bottom: 50,
    }
})